import java.util.ArrayList;

import dao.Bd;
import dao.DaoCategorie;
import modele.*;
import vue.*;

public class main {
	public static VueAccueil vueAccueil;
	public static void main(String[] args) {
		try {
			// Cr�ation de la vue principale
			
			Bd.init();
			ArrayList<Categorie> listeCategorie = new ArrayList<Categorie>();
			listeCategorie.add(new Categorie(1,"Immobilier"));
			listeCategorie.add(new Categorie(2,"Automobile"));
			vueAccueil = new VueAccueil(DaoCategorie.listeCategories());
			vueAccueil.setVisible(true);
			
			
			
		} catch (Exception ex) {
			System.out.println("Erreur: "+ex);
			System.exit(1);
		}
		
	}
}
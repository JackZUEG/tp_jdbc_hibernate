package dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import modele.Annonce;
import modele.Utilisateur;

public class DaoAnnonce {
	public static ArrayList<Annonce> listeAnnonces(String nomCategorie, String prixMin, String prixMax, String localisation){
		ArrayList<Annonce> listeAnnonces = new ArrayList<Annonce>();
		try {
			String requete = "SELECT idCategorie from categorie where nom=?;";
			PreparedStatement stmt = Bd.getConnection().prepareStatement(requete);
			stmt.setString(1, nomCategorie);
			int idCategorie = 0;
			ResultSet resultCategorie = stmt.executeQuery();
			
			while(resultCategorie.next()) {
				idCategorie = resultCategorie.getInt("idCategorie");
			}
			
			requete = "SELECT * from annonce a, offre o where a.idAnnonce = o.idAnnonce and o.acceptation IS NOT NULL and a.idCategorie="+idCategorie;
			if(!prixMin.isEmpty()) {
				System.out.println();
				requete = requete+" and "+Integer.parseInt(prixMin)+"<=prix";
				
			}
			if(!prixMax.isEmpty()) {
				requete = requete+" and "+Integer.parseInt(prixMax)+">=prix";
			}
			if(!localisation.isEmpty()) {
				requete = requete+" and '"+localisation+"'=position;";
			}
			ResultSet result = stmt.executeQuery(requete);

			while(result.next()) {
				
				listeAnnonces.add(new Annonce(result.getInt("idAnnonce"), result.getString("type"),result.getString("position"),result.getFloat("prix"),result.getString("description"),result.getDate("datePublication"),-1,null));	
			}
			
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listeAnnonces;
	}
	
	public static Annonce detailsAnnonce(int idAnnonce) {
		Annonce a = null;
		try {
			String requete = "SELECT * from annonce where idAnnonce=?;";
			PreparedStatement stmt = Bd.getConnection().prepareStatement(requete);
			stmt.setInt(1, idAnnonce);
			ResultSet result = stmt.executeQuery();
			
			while(result.next()) {
				String type = result.getString("type");
				String position = result.getString("position");
				Float prix = result.getFloat("prix");
				String description = result.getString("description");
				Date date = result.getDate("datePublication");
				
				a = new Annonce(idAnnonce,type,position,prix,description,date,-1,null);
			}
			
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return a;
	}
	
	public static void ajouterAnnonce(Annonce annonce) {
		
		String requeteDernierID = "SELECT idAnnonce FROM annonce ORDER BY idAnnonce DESC LIMIT 1";
		String requeteIDCategorie = "SELECT idCategorie FROM Categorie WHERE nom = ?";
		String requeteAjout = "INSERT INTO annonce(idAnnonce, type, position, prix, description, datePublication, idCategorie, idVendeur) VALUES(?,?,?,?,?,?,?,?)";
		
		try {
			//recupere dernier id des annonces en bd
			PreparedStatement stmt = Bd.getConnection().prepareStatement(requeteDernierID);
			ResultSet rs = stmt.executeQuery();
			if(rs.next())
				annonce.setIdAnnonce( rs.getInt(1)+1);
			else
				annonce.setIdAnnonce(0);
			
			//recupere id de la catégorie
			stmt = Bd.getConnection().prepareStatement(requeteIDCategorie);
			stmt.setString(1, annonce.getCategorie().getNom());
			rs = stmt.executeQuery();
			rs.next();
			int idCategorie = rs.getInt(1);
			
			stmt = Bd.getConnection().prepareStatement(requeteAjout);
			stmt.setInt(1, annonce.getIdAnnonce());
			stmt.setString(2, annonce.getType());
			stmt.setString(3, annonce.getPosition());
			stmt.setFloat(4, annonce.getPrix());
			stmt.setString(5, annonce.getDescription());
			stmt.setDate(6, annonce.getDatePublication());
			stmt.setInt(7, idCategorie);
			stmt.setInt(8, annonce.getIdUtilisateur());
			
			stmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static ArrayList<Annonce> listeMesAnnonces(Utilisateur utilisateur) {
		ArrayList<Annonce> listeAnnonces = new ArrayList<Annonce>();
		String requete = "SELECT idAnnonce FROM annonce where idVendeur=?";
		try {
			PreparedStatement stmt = Bd.getConnection().prepareStatement(requete);
			stmt.setInt(1, utilisateur.getIdUtilisateur());
			ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				listeAnnonces.add(detailsAnnonce(rs.getInt("idAnnonce")));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listeAnnonces;
	}
	
	public static ArrayList<Annonce> listeAnnoncesMesOffres(Utilisateur utilisateur) {
		ArrayList<Annonce> listeAnnonces = new ArrayList<Annonce>();
		String requete = "SELECT idAnnonce FROM annonce WHERE idAnnonce IN (SELECT o.idAnnonce FROM offre o WHERE o.idUtilisateur = ? and o.acceptation IS NULL)";
		try {
			PreparedStatement stmt = Bd.getConnection().prepareStatement(requete);
			stmt.setInt(1, utilisateur.getIdUtilisateur());
			ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				listeAnnonces.add(detailsAnnonce(rs.getInt("idAnnonce")));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listeAnnonces;
	}
}

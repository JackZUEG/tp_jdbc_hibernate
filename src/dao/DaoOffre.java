package dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;

import modele.Annonce;
import modele.Offre;
import modele.Utilisateur;

public class DaoOffre {
	public static boolean verifExisteOffre(Offre offre){ 
		String requete  = "SELECT idAnnonce, idUtilisateur from Offre where idUtilisateur=? and idAnnonce=?";
		boolean existe = false;
		try {
			PreparedStatement stmt = Bd.getConnection().prepareStatement(requete);
			stmt.setInt(1, offre.getDemandeur().getIdUtilisateur());
			stmt.setInt(2,  offre.getAnnonce().getIdAnnonce());
			ResultSet rs = stmt.executeQuery();
			if(rs.next()) {
				existe = true;
			}

			stmt.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return existe;
	}
	
	public static void creationOffre(Offre offre){ 
		String requete  = "INSERT INTO Offre VALUES(?,?,?,?)";
		try {
			PreparedStatement stmt = Bd.getConnection().prepareStatement(requete);
			stmt.setInt(1, offre.getDemandeur().getIdUtilisateur());
			stmt.setInt(2,  offre.getAnnonce().getIdAnnonce());
			stmt.setDate(3, offre.getDateOffre());
			stmt.setDate(4,offre.getAcceptation());
			stmt.executeUpdate();

			stmt.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
		
	public static ArrayList<Annonce> listeMesOffres(Utilisateur utilisateur) {
		ArrayList<Annonce> listeOffres = new ArrayList<Annonce>();
		String requete = "SELECT * FROM offre where idUtilisateur=? ORDER BY idAnnonce DESC LIMIT 1";
		try {
			PreparedStatement stmt = Bd.getConnection().prepareStatement(requete);
			stmt.setInt(1, utilisateur.getIdUtilisateur());
			ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				//listeOffres.add(detailsAnnonce(rs.getInt("idVendeur")));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listeOffres;
	}
	
	public static ArrayList<Offre> listeOffresAnnonce(int idAnnonce){
		ArrayList<Offre> listeOffres = new ArrayList<Offre>();
		String requete = "SELECT * FROM offre where idAnnonce=?";
		try {
			PreparedStatement stmt = Bd.getConnection().prepareStatement(requete);
			stmt.setInt(1, idAnnonce);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				listeOffres.add(new Offre(rs.getDate(3), 
										  rs.getDate(4), 
										  DaoAnnonce.detailsAnnonce(rs.getInt(2)), 
										  DaoUtilisateur.getUtilisateur(rs.getInt(1))));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listeOffres;
	}
	
	
	public static void validerOffre(Offre offre) {
		String requeteAccepter = "UPDATE offre SET acceptation = ? WHERE idAnnonce = ? and idUtilisateur=?";
		String requeteSupprimer = "DELETE FROM offre WHERE idAnnonce = ? and acceptation IS NULL";
		try {
			//ajout de la date d'acceptation
			PreparedStatement stmt = Bd.getConnection().prepareStatement(requeteAccepter);
			stmt.setDate(1, new Date(Calendar.getInstance().getTime().getTime()));
			stmt.setInt(2, offre.getAnnonce().getIdAnnonce());
			stmt.setInt(3, offre.getDemandeur().getIdUtilisateur());
			
			stmt.executeUpdate();
			
			//Supression des offres qui ne sont pas accept�
			stmt = Bd.getConnection().prepareStatement(requeteSupprimer);
			stmt.setInt(1, offre.getAnnonce().getIdAnnonce());
			
			stmt.executeUpdate();
			
			stmt.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void accepterOffre(Offre offre) {
		String requete = "UPDATE offre SET acceptation=? where idAnnonce=? and idUtilisateur=?";
		try {
			PreparedStatement stmt = Bd.getConnection().prepareStatement(requete);
			stmt.setDate(1, new java.sql.Date(Calendar.getInstance().getTime().getTime()));
			stmt.setInt(2, offre.getAnnonce().getIdAnnonce());
			stmt.setInt(3, offre.getDemandeur().getIdUtilisateur());
			stmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void supprimerOffres(Annonce annonce) {
		String requete = "DELETE FROM offre where idAnnonce=?";
		try {
			PreparedStatement stmt = Bd.getConnection().prepareStatement(requete);
			stmt.setInt(1, annonce.getIdAnnonce());
			stmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}

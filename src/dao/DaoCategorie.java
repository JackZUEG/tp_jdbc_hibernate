package dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import modele.Categorie;

public class DaoCategorie {
	/*
	 * renvoie la liste des categories pour le filtre dans la vueAccueil
	 */
	public static ArrayList<Categorie> listeCategories(){
		
		ArrayList<Categorie> listeCategories = new ArrayList<Categorie>();	
		try {
			Statement stmt = Bd.getConnection().createStatement();
			ResultSet result = stmt.executeQuery("SELECT * from Categorie;");

			while(result.next()) {
				listeCategories.add(new Categorie(result.getInt("idCategorie"),result.getString("nom")));	
			}
			
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listeCategories;
	}
}

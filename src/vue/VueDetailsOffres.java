package vue;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import controleur.ValiderOffreControleur;
import dao.DaoAnnonce;
import modele.Annonce;
import modele.Offre;
import modeleTables.OffresTableModele;

public class VueDetailsOffres extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	OffresTableModele lesOffresAnnonces;
	JButton validerOffre;
	Offre offreSelectionne;
	JTable tableOffre;
	
	public VueDetailsOffres(ArrayList<Offre> listeOffres) {
		
		setContentPane(createContent(listeOffres));
		
		setTitle("Liste des Offres de l'annonces");
		
		setPreferredSize(new Dimension(600, 480));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
	}

	private Container createContent(ArrayList<Offre> listeOffres) {
		JPanel container = new JPanel();
		tableOffre = new JTable();
		ValiderOffreControleur validerControler = new ValiderOffreControleur(this);
		
		container.setLayout(new BorderLayout());
		lesOffresAnnonces = new OffresTableModele(listeOffres);

		tableOffre.getTableHeader().setReorderingAllowed(false);
		tableOffre.setModel(lesOffresAnnonces);
		tableOffre.addMouseListener(validerControler);
		
		validerOffre = null;
		if(!(listeOffres.size() == 1 && listeOffres.get(0).getAcceptation() != null)) {
			validerOffre = new JButton("Valider Offre");
			validerOffre.setEnabled(false);
			validerOffre.addActionListener(validerControler);
			container.add(validerOffre, BorderLayout.SOUTH);
		}
		
		container.add(new JScrollPane(tableOffre), BorderLayout.CENTER);
		
		
		return container;
	}
	
	
	public void enableButton() {
		if(validerOffre != null)
			validerOffre.setEnabled(true);
	}
	
	public Offre getOffreSelectionnee() {
		if (tableOffre.getSelectedRow() != -1)  // On v�rifie si une ligne est s�lectionn�e
        	return lesOffresAnnonces.getOffre(tableOffre.getSelectedRow());
		else
			return null;
	}
	
	
}

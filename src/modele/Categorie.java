package modele;

/**
 * Classe représentant l'objet Catégorie
 */
public class Categorie {
	private int idCategorie;
	private String nom;
	
	/**
	 * Constructeur de la classe catégorie
	 */
	public Categorie(int idCategorie, String nom) {
		this.idCategorie = idCategorie;
		this.nom = nom;
	}

	public int getIdCategorie() {
		return idCategorie;
	}

	public String getNom() {
		return nom;
	}
	
	
}

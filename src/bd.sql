-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  ven. 18 déc. 2020 à 10:23
-- Version du serveur :  5.7.17
-- Version de PHP :  5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `tp4cc-jdbc`
--

-- --------------------------------------------------------

--
-- Structure de la table `annonce`
--

CREATE TABLE `annonce` (
  `idAnnonce` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `position` varchar(255) NOT NULL,
  `prix` float NOT NULL,
  `description` text NOT NULL,
  `datePublication` date NOT NULL,
  `idCategorie` int(11) NOT NULL,
  `idVendeur` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `annonce`
--

INSERT INTO `annonce` (`idAnnonce`, `type`, `position`, `prix`, `description`, `datePublication`, `idCategorie`, `idVendeur`) VALUES
(1, 'Maison 7 pièces 175 m carres', 'Paris', 473800, 'ORLEANS COLIGNY - COEUR DE JARDINS - SUPERBE MAISON FAMILIALE 1920, de 175m2 avec son jardin clos et 2 garages\r\nUn exceptionnel mélange d\'ancien et de moderne. Maison 1920 agrandie en 2015 et 2016, permettant aujourd\'hui une vie entièrement de plain pied tournée vers le jardin SUD et OUEST. Elle comprend en RDC ; 2 chambres et SDB + grand dressing, Superbes pièces de vie avec cuisine -verrière et salle à manger, puis salon. Ouvrant chacun sur une terrasse séparée et le jardin clos.', '2020-12-17', 1, 1),
(2, 'Appartement 5 pièces 100 m²', 'Tours', 155000, 'Appartement Beaugency 5 pièce(s) 100.67 m2\r\n\r\nSitué à Beaugency dans le centre ville, ce grand appartement avec colombages est ouvert sur une terrasse jardin exposé ouest . Composé d\'une grande pièce à vivre dont un salon avec cheminée, une cuisine centrale, . 2 chambres avec chacune une salle de bains aménagée, bureau avec placard, 2 wc. Chaufferie.\r\nRéférence annonce : E1O9O7\r\nLe prix indiqué comprend les honoraires à la charge de l\'acheteur : 6,90% TTC du prix du bien hors honoraires\r\nPrix hors honoraires : 145 000 €\r\n\r\nA propos de la copropriété :\r\nNombre de lots : 3\r\nCharges prévisionnelles annuelles : 480 €', '2020-12-18', 1, 2),
(3, 'Clio 3 estate tce', 'tours', 3900, 'chalette pièces auto\r\n65 rue andré gide\r\n45120 chalette sur loing\r\n0218124518\r\ncpa@sfr.fr\r\n\r\nvous propose\r\nclio 3 estate (break)\r\nannée 2011\r\nenergie essence\r\nmodèle DYNAMIQUE\r\ntce 100 eco 2\r\nversion tom tom (gps)\r\ncontrôle technique ok\r\ngarantie moteur boite de vitesse 3 mois\r\navec attelages\r\nrévision faite', '2020-12-09', 2, 3),
(4, 'Dvd \" LES 12 CHIENS DE NOEL \"', 'nice', 10, '* NEUF * SOUS CELLOPHANE\r\nDVD \"LES 12 CHIENS DE NOEL\"\r\nUNE MAGNIFIQUE HISTOIRE DE NOEL DANS LAQUELLE GRACE A L\'AMITIE ET L\'ESPOIR, RIEN N\'EST JAMAIS PERDU. CETTE AVENTURE POUR TOUTE LA FAMILLE, MELANT EMOTION, SUSPENSE ET HUMOUR, A ETE UN ENORME SUCCES DVD\r\nDurée : 103 mn, langue française, son stéréo.', '2020-12-10', 4, 4);

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

CREATE TABLE `categorie` (
  `idCategorie` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`idCategorie`, `nom`) VALUES
(1, 'immobilier'),
(2, 'automobile'),
(3, 'Multimedia'),
(4, 'Loisirs'),
(5, 'Mode'),
(6, 'Maison');

-- --------------------------------------------------------

--
-- Structure de la table `offre`
--

CREATE TABLE `offre` (
  `idUtilisateur` int(11) NOT NULL,
  `idAnnonce` int(11) NOT NULL,
  `dateOffre` date DEFAULT NULL,
  `acceptation` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `offre`
--

INSERT INTO `offre` (`idUtilisateur`, `idAnnonce`, `dateOffre`, `acceptation`) VALUES
(4, 2, '2020-12-09', NULL),
(5, 2, '2020-12-10', NULL),
(5, 1, '2020-12-09', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `idUtilisateur` int(11) NOT NULL,
  `identifiant` varchar(255) NOT NULL,
  `mdp` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`idUtilisateur`, `identifiant`, `mdp`) VALUES
(1, 'user1', 'user1'),
(2, 'user2', 'user2'),
(3, 'user3', 'user3'),
(4, 'user4', 'user4'),
(5, 'user5', 'user5');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `annonce`
--
ALTER TABLE `annonce`
  ADD PRIMARY KEY (`idAnnonce`),
  ADD KEY `FK_vendeurAnnonce` (`idVendeur`),
  ADD KEY `FK_categorieAnnonce` (`idCategorie`);

--
-- Index pour la table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`idCategorie`);

--
-- Index pour la table `offre`
--
ALTER TABLE `offre`
  ADD PRIMARY KEY (`idUtilisateur`,`idAnnonce`),
  ADD KEY `idAnnonce` (`idAnnonce`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`idUtilisateur`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `annonce`
--
ALTER TABLE `annonce`
  MODIFY `idAnnonce` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `categorie`
--
ALTER TABLE `categorie`
  MODIFY `idCategorie` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `idUtilisateur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

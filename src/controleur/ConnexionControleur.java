package controleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.Timer;

import dao.DaoUtilisateur;
import modele.Utilisateur;
import vue.VueAccueil;
import vue.VueAccueilAuthentifie;

public class ConnexionControleur implements ActionListener{
	
	private VueAccueilAuthentifie vue;
	private VueAccueil vueAccueil;

	public ConnexionControleur(VueAccueilAuthentifie vue, VueAccueil vueAccueil) {
		super();
		this.vue = vue;
		this.vueAccueil = vueAccueil;
	}



	@Override
	public void actionPerformed(ActionEvent e) {
		String nom = this.vue.getChampsNom().getText();
		String mdp = this.vue.getChampsMdp().getText();
		try {
			Utilisateur user = DaoUtilisateur.getUtilisateur(nom, mdp);
			this.vue.setInformation("Vous �tes connect�");
			
			//GERER LA CONNEXION DANS LA VUE
			
			fermerVue();
		
			vueAccueil.setVisibleConnexion(user);
			vueAccueil.setVisible(true);

			
		} catch (Exception e1) {
			this.vue.setInformation(e1.getMessage());
		}
		
	}
	
	private void fermerVue() {
		Timer timer = new Timer (1000, new AbstractAction() {
			  /**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			  public void actionPerformed(ActionEvent ae) {
			    vue.dispose();
			  }
			});
		timer.setRepeats(false);//the timer should only go off once
		timer.start();
	}


}
